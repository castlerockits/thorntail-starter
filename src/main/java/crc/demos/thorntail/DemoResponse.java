package crc.demos.thorntail;

import java.util.Date;

public class DemoResponse
{
   private Date date;

   public Date getDate ()
   {
      return date;
   }

   public void setDate ( Date date )
   {
      this.date = date;
   }
}
