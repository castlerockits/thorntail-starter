package crc.demos.thorntail;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/demo")
public class DemoResource
{
   @Inject DemoService demoService;

   @GET
   @Produces(MediaType.APPLICATION_JSON)
   public DemoResponse getDemoResponse ()
   {
      return demoService.createNewResponse();
   }
}
