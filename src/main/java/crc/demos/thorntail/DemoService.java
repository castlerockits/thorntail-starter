package crc.demos.thorntail;

import java.util.Date;

public class DemoService
{

   public DemoResponse createNewResponse ()
   {
      DemoResponse response = new DemoResponse();
      response.setDate( new Date() );
      return response;
   }
}
