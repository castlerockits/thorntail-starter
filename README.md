# Thorntail Starter

This project provides a template for running a [Thorntail](https://thorntail.io/) Java EE application.

To run the application execute

```
mvn thorntail:run
```

or

```
mvn package && java -jar target/thorntail-starter-1.0-thorntail.jar
```

Then use `curl` or go to [http://localhost:8080/demo](http://localhost:8080/demo) in a web browser.